/**************************
 * Author: Akhil Acharya
 * Date Started: 3/9/14
 * Description: Counts Lines of Code for a particular folder
 ****************************/

var walk = require('walk');
var fs = require('fs');

var files = [];

var readline = require('readline');

var config = require('./config'); 


var reader = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
});

var totalLines = 0;
var totalFiles = 0;

reader.question("What root folder would you like to include?\n", function(answer) {

    var walker = walk.walk(answer, {
        followLinks: false
    });

    var update = function(){
        console.log('Working...'); 
    }; 

    var intervalId = setInterval(update, 3*1000); 
    update(); 

    walker.on('file', function(root, stat, next) {
        var file = root + "/" + stat.name;
        if (hasKey(file)) {
            totalFiles++;
            var lines = fs.readFileSync(file, 'utf8').split('\n').length;
            totalLines += lines;
        }
        next();
    });

    walker.on('end', function() {
        console.log(); 
        console.log(totalLines + " is the number of total lines");
        console.log(totalFiles + " is the number of total files");
        clearInterval(intervalId);
        reader.close();  
    });

})


function hasKey(file) {
    num_keys = 0;
    
    config.include.forEach(function(key) {
        if (file.indexOf(key) != -1) {
            num_keys++;
        }
    });
    config.ignore.forEach(function(key) {
        if (file.indexOf(key) != -1) {
            num_keys = 0;
        }
    });

    return num_keys > 0;
}
