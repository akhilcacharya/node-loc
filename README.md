#Node-LOC

When using grep is just too darn hard. 

Easily counts the number of lines of code in any project by recursively walking through directories. You can also configure the script to exclude or include particular filetypes. 

#Dependencies

* Walk

#License

MIT